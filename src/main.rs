use serde::{Deserialize, Serialize};
use std::{
    collections::{BTreeMap, HashMap},
    io::{BufRead, BufReader, Write},
    os::unix::ffi::OsStrExt,
    path::{Path, PathBuf},
    process::{Child, ChildStdin, ChildStdout},
    fmt::Write as _,
};

type Result<T, E = anyhow::Error> = std::result::Result<T, E>;
#[allow(dead_code)]
struct Symbolizer {
    child: Child,
    tx: ChildStdin,
    rx: BufReader<ChildStdout>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Symbol {
    column: u32,
    discriminator: u32,
    file_name: String,
    start_file_name: String,
    start_address: String,
    function_name: String,
    line: u32,
    start_line: u32,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct LlvmSymbolizerError {
    message: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct SymbolInfo {
    address: String,
    module_name: PathBuf,
    #[serde(default)]
    symbol: Vec<Symbol>,
    #[serde(default)]
    error: Option<LlvmSymbolizerError>,
}

impl Symbolizer {
    fn new() -> Result<Self> {
        let mut child = std::process::Command::new("llvm-symbolizer")
            .arg("--relative-address")
            .arg("--relativenames")
            .arg("--output-style=JSON")
            .stdin(std::process::Stdio::piped())
            .stdout(std::process::Stdio::piped())
            .spawn()?;
        let tx = child.stdin.take().unwrap();
        let rx = child.stdout.take().unwrap();
        Ok(Self {
            child,
            tx,
            rx: BufReader::new(rx),
        })
    }
    fn symbolize(&mut self, obj: &Path, addr: u64) -> Result<SymbolInfo> {
        self.tx.write_all(obj.as_os_str().as_bytes())?;
        self.tx.write_all(format!(" {:#x}\n", addr).as_bytes())?;
        let mut json = String::new();
        self.rx.read_line(&mut json)?;
        Ok(serde_json::from_str(&json).expect(&format!("{json} {:?} {addr}", obj.as_os_str().as_bytes())))
    }
}
mod parsers {
    use nom::{
        branch::alt,
        bytes::complete::{is_not, take_while_m_n},
        character::complete::char,
        combinator::{map, map_opt, map_res, value, verify},
        error::{FromExternalError, ParseError},
        multi::fold_many0,
        sequence::{delimited, preceded},
        IResult, Parser,
    };

    /// Parse a unicode sequence, of the form XXXX, where XXXX is 1 to 6
    /// hexadecimal numerals.
    fn parse_unicode<'a, E>(input: &'a str) -> IResult<&'a str, char, E>
    where
        E: ParseError<&'a str> + FromExternalError<&'a str, std::num::ParseIntError>,
    {
        let parse_hex = take_while_m_n(1, 6, |c: char| c.is_ascii_hexdigit());
        let parse_u32 = map_res(parse_hex, move |hex| u32::from_str_radix(hex, 16));

        map_opt(parse_u32, std::char::from_u32).parse(input)
    }

    /// Parse an escaped character: \n, \t, \r, \00AC, etc.
    fn parse_escaped_char<'a, E>(input: &'a str) -> IResult<&'a str, char, E>
    where
        E: ParseError<&'a str> + FromExternalError<&'a str, std::num::ParseIntError>,
    {
        preceded(
            char('\\'),
            alt((
                parse_unicode,
                value('\n', char('n')),
                value('\r', char('r')),
                value('\t', char('t')),
                value('\\', char('\\')),
                value('/', char('/')),
                value('"', char('"')),
            )),
        )
        .parse(input)
    }

    /// Parse a non-empty block of text that doesn't include \ or "
    fn parse_literal<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, &'a str, E> {
        let not_quote_slash = is_not("\"\\");

        verify(not_quote_slash, |s: &str| !s.is_empty()).parse(input)
    }

    /// A string fragment contains a fragment of a string being parsed: either
    /// a non-empty Literal (a series of non-escaped characters), a single
    /// parsed escaped character, or a block of escaped whitespace.
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    enum StringFragment<'a> {
        Literal(&'a str),
        EscapedChar(char),
    }

    /// Combine parse_literal, parse_escaped_whitespace, and parse_escaped_char
    /// into a StringFragment.
    fn parse_fragment<'a, E>(input: &'a str) -> IResult<&'a str, StringFragment<'a>, E>
    where
        E: ParseError<&'a str> + FromExternalError<&'a str, std::num::ParseIntError>,
    {
        alt((
            // The `map` combinator runs a parser, then applies a function to the output
            // of that parser.
            map(parse_literal, StringFragment::Literal),
            map(parse_escaped_char, StringFragment::EscapedChar),
        ))
        .parse(input)
    }

    /// Parse a string. Use a loop of parse_fragment and push all of the fragments
    /// into an output string.
    pub fn parse_string(input: &str) -> IResult<&str, String> {
        let build_string = fold_many0(parse_fragment, String::new, |mut string, fragment| {
            match fragment {
                StringFragment::Literal(s) => string.push_str(s),
                StringFragment::EscapedChar(c) => string.push(c),
            }
            string
        });

        delimited(char('"'), build_string, char('"')).parse(input)
    }
}

fn wine_path_to_unix_path(wine_prefix: &Path, path: &typed_path::WindowsPath) -> Option<PathBuf> {
    let mut components = path.components();
    let typed_path::WindowsComponent::Prefix(prefix) = components.next()? else {
        return None;
    };
    match prefix.kind() {
        typed_path::WindowsPrefix::Disk(b'C') | typed_path::WindowsPrefix::Disk(b'c') => {
            let _root_dir = components.next()?;
            let mut path = wine_prefix.join("drive_c");
            path.extend(
                components.map(|f| <_ as TryInto<std::path::Component<'_>>>::try_into(f).unwrap()),
            );
            Some(path)
        }
        typed_path::WindowsPrefix::Disk(b'Z') | typed_path::WindowsPrefix::Disk(b'z') => Some(
            components
                .map(|f| <_ as TryInto<std::path::Component<'_>>>::try_into(f).unwrap())
                .collect(),
        ),
        _ => None,
    }
}

struct Mapping {
    start: u64,
    end: u64,
}
impl PartialEq for Mapping {
    fn eq(&self, other: &Self) -> bool {
        self.start == other.start
    }
}
impl PartialOrd for Mapping {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Eq for Mapping {}
impl Ord for Mapping {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.start.cmp(&other.start)
    }
}

fn main() -> Result<()> {
    let wine_prefix = std::env::var("WINEPREFIX")
        .map(|x| Path::new(&x).to_owned())
        .or_else(|_| std::env::var("HOME").map(|x| Path::new(&x).join(".wine")))?;

    let mapping_regex = regex::Regex::new(
        r"map_image_into_view mapping PE file L(.*) at 0x([0-9a-f]+)-0x([0-9a-f]+)",
    )?;
    let addr_regex = regex::Regex::new(r"(?:0x)?([0-9a-fA-F]+)")?;
    let mut symbolizer = Symbolizer::new().expect("Failed to start symbolizer");
    let mut pid_mappings: HashMap<_, BTreeMap<Mapping, PathBuf>> = HashMap::new();
    let mut missing_pid_warned = false;
    for line in std::io::stdin().lines() {
        let line = line?;
        let mut headers = line.split(':');
        // Try to get pid and tid from wine log headers, if they are not present, assume
        // the line is not a log line and print it as-is.
        let Some(maybe_pid) = headers.next() else {
            println!("{}", line);
            continue;
        };
        let Some(maybe_tid) = headers.next() else {
            println!("{}", line);
            continue;
        };
        if maybe_pid.len() != 4 || maybe_tid.len() != 4 {
            println!("{}", line);
            continue;
        }
        // The first field is always an id, so if it can't be parsed as one, assume
        // the line is not a log line and print it as-is.
        let Ok(first_id) = u64::from_str_radix(maybe_pid, 16) else {
            println!("{}", line);
            continue;
        };
        let pid = if let Ok(_second_id) = u64::from_str_radix(maybe_tid, 16) {
            Some(first_id)
        } else {
            if !missing_pid_warned {
                eprintln!("Missing pid in wine log, symbolization might be inaccurate. To fix this problem, run wine with `WINEDEBUG=+pid`.");
                missing_pid_warned = true;
            }
            None
        };
        if let Some(cap) = mapping_regex.captures(&line) {
            let path = &cap[1];
            let (_, path) = parsers::parse_string(path).map_err(|x| x.to_owned())?;
            let path = path.strip_prefix("\\??\\").unwrap_or(&path);
            let path = path.to_ascii_lowercase();
            let path = typed_path::WindowsPath::new(&path);
            let start = u64::from_str_radix(&cap[2], 16)?;
            let end = u64::from_str_radix(&cap[3], 16)?;
            println!("{} {start:#x} {end:#x}", path.display());
            let Some(unix_path) = wine_path_to_unix_path(&wine_prefix, path) else {
                continue;
            };
            println!("{}", unix_path.display());
            pid_mappings
                .entry(pid)
                .or_default()
                .insert(Mapping { start, end }, unix_path);
        } else {
            let new_line = addr_regex.replace_all(&line, |caps: &regex::Captures| {
                let addr = u64::from_str_radix(&caps[1], 16).unwrap();
                if let Some((mapping, obj)) = pid_mappings.get(&pid).and_then(|mappings| {
                    mappings
                        .range(
                            ..=Mapping {
                                start: addr,
                                end: addr,
                            },
                        )
                        .next_back()
                }) {
                    if mapping.end <= addr {
                        return caps[1].to_owned();
                    }

                    let result = symbolizer.symbolize(obj, addr - mapping.start).unwrap();
                    if let Some(error) = result.error {
                        eprintln!("Error: {} ({} {:#x})", error.message, obj.display(), addr - mapping.start);
                    }
                    let symbols: Vec<_> = result
                        .symbol
                        .into_iter()
                        .filter(|s| !s.function_name.is_empty())
                        .collect();
                    if !symbols.is_empty() {
                        let mut info = String::new();
                        for symbol in symbols {
                            if !info.is_empty() {
                                write!(info, " <- ").unwrap();
                            }
                            if symbol.file_name.is_empty() {
                                write!(info, "{}", symbol.function_name).unwrap();
                            } else {
                                write!(info, "{}, {}:{},{}", symbol.function_name, symbol.file_name, symbol.line, symbol.column).unwrap();
                            }
                        }
                        format!("{} ({info})", &caps[1])
                    } else {
                        format!(
                            "{} ({}+{:#x})",
                            &caps[1],
                            obj.file_name().unwrap().to_string_lossy(),
                            addr - mapping.start
                        )
                    }
                } else {
                    caps[1].to_owned()
                }
            });
            println!("{new_line}");
        }
    }
    if pid_mappings.is_empty() {
        eprintln!("No mappings found, did you run wine with `WINEDEBUG=+module`?");
    }
    Ok(())
}
