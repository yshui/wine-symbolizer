# wine-symbolizer

Add function names and source file names to wine logs.

This program goes through a wine log file and searchs for anything that looks like a address, then it uses `llvm-symbolizer` to resolve and function name and a source file name for the address.

## Getting started

To generate a log file that can be understood by `wine-symbolizer`, you need to run wine with:

```bash
WINEDEBUG=+module,+pid wine ...
```

This program also looks for wine dlls in `WINEPREFIX`, so you need to set it appropriately.
